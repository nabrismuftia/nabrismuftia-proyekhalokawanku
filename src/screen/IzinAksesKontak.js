import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

const IzinAksesKontak = ({navigation}) => {
  useEffect(() => {
    ToastAndroid.showWithGravity(
      'Login berhasil',
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  }, []);
  return (
    <View style={{flex: 1, alignItems: 'center'}}>
      <Image
        style={styles.imgSplash}
        source={require('../asset/img-splash.png')}
      />
      <View style={styles.container}>
        <View style={styles.headerText}>
          <Text style={styles.textLogin}>
            Halo Kawanku{'\n'}Perlu Mengakses Kontakmu
          </Text>
          <ScrollView style={styles.scrollView}>
            <Text style={{fontWeight: '300'}}>
              Untuk mempermudah proses koneksi dengan rekan-rekanmu yang ekstra
              beken, Halo Kawanku perlu mengakses kontak dari ponsel.
            </Text>
          </ScrollView>
          <TouchableOpacity
            onPress={() => navigation.navigate('ModalLoading')}
            style={styles.tombolLogin}>
            <Text style={styles.textTombolLogin}>Izinkan</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgSplash: {
    width: '100%',
    height: '100%',
  },
  container: {
    marginTop: '-120%',
    flexDirection: 'column',
  },
  headerText: {
    backgroundColor: 'white',
    width: 300,
    padding: 20,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  scrollView: {height: 80},
  iconLoginViaGoogle: {
    height: 20,
    width: 20,
    margin: 12,
  },
  textLogin: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    marginVertical: 10,
  },
  txtInput: {
    width: '100%',
    marginTop: 10,
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  tombolLogin: {
    borderWidth: 1,
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  tombolLoginViaGoogle: {
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  textTombolLogin2: {
    flexDirection: 'row',
    marginTop: 20,
    alignSelf: 'center',
  },
  tombolLogin: {
    borderWidth: 1,
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
});
export default IzinAksesKontak;
