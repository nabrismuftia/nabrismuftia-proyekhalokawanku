import React, {useEffect} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const Splashscreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login');
    }, 2000);
  }, []);

  return (
    <View style={styles.container}>
      <Image
        style={styles.imgSplash}
        source={require('../asset/img-splash.png')}
      />
      <View style={styles.container2}>
        <Text style={styles.textHaloKawanku}>Halo{'\n'}Kawanku </Text>
        <Text style={{fontSize: 12}}> Sarana Terhubung dengan Kawan Lama</Text>
        <Image
          style={styles.desainGrafisOleh}
          source={require('../asset/img-desain-grafis-oleh.png')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  container2: {
    marginTop: '-110%',
    flexDirection: 'column',
  },
  imgSplash: {
    width: '100%',
    height: '100%',
  },
  textHaloKawanku: {
    fontSize: 48,
    fontWeight: 'bold',
    color: 'green',
  },
  desainGrafisOleh: {
    width: 200,
    height: 52,
    marginTop: 25,
  },
});
export default Splashscreen;
