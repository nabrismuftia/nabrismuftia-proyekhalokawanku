import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Clipboard,
  ToastAndroid,
} from 'react-native';
import {BASE_URL, TOKEN} from '../utils/service/Url';
import Axios from 'axios';
import {useState} from 'react';

const Separator = () => <View style={styles.separator} />;

const Profile = ({navigation, route}) => {
  const [contact, setContact] = useState(null);
  const item = route.params.item;
  const noHape = item?.id.substring(0, 10);
  console.log('isi item di Profile.js', item);
  const itemID = item?.id;

  const copyToClipboard = () => {
    Clipboard.setString(noHape);
    ToastAndroid.showWithGravity(
      `${noHape} copied to clipboard`,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  };

  const deleteContact = async () => {
    const body = [
      {
        id: item.id,
        age: item.age,
        firstName: item.firstName,
        lastName: item.lastName,
        photo: item.photo,
      },
    ];
    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };

    Axios.delete(`${BASE_URL}contact/${item.id}`, {data: body, ...options})
      .then(response => {
        console.log('response delete success', response);
        ToastAndroid.showWithGravity(
          'Sebuah kontak kawan baru berhasil dihapus',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
        navigation.navigate('Home');
      })
      .catch(error => {
        console.log('error delete', error);
        console.log('Response status:', error.response.status);
        console.log('Response data:', error.response.data);
        console.log('Response headers:', error.response.headers);
        ToastAndroid.showWithGravity(
          'Error menghapus kawan baru',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
      });
  };

  return (
    <View style={styles.container}>
      {/* Header dimulai dari sini */}
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            source={require('../asset/icon-back.png')}
            style={styles.iconBack}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>Profile Kawanku</Text>
        <Image
          source={require('../asset/icon-back.png')}
          style={styles.fakeIcon}
        />
      </View>
      {/* Header Selesai di sini  */}
      <ScrollView style={styles.scrollView}>
        <View style={styles.body}>
          <View style={styles.containerPP}>
            <Image source={{uri: item?.photo}} style={styles.char} />
            <Image source={require('../asset/char-2.png')} style={styles.pp} />
          </View>
          <View style={styles.profileUtama}>
            <View style={styles.containerColumn}>
              <Text style={styles.textNamaLengkap}>
                {item?.firstName} {item?.lastName}
              </Text>
              <Text style={styles.textUmurDanKota}>
                {item?.age} tahun, Surakarta
              </Text>
              <View style={styles.editAndDelete}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('EditKontak', (route = {route}))
                  }>
                  <Text>Edit</Text>
                </TouchableOpacity>
                <Text> | </Text>
                <TouchableOpacity onPress={deleteContact}>
                  <Text style={styles.textDelete}>Delete</Text>
                </TouchableOpacity>
              </View>
              <Separator />
              <Text style={styles.textDeskripsi}>
                Tentang Kawanku: Seorang{' '}
                <Text style={styles.textBold}>Programmer </Text>
                di{'\n'}
                <Text style={styles.textBold}> CODEMASTERS.ID</Text> yang beken
                dan trendi. Kami bertemu di bootcamp sejak bulan April 2023. Dia
                berjanji akan menjadi seorang jagoan coding sejagat nusantara
                suatu saat nanti.
              </Text>
            </View>
          </View>
          <Separator />
          {/* Barisan Nomor Hape dimulai dari sini */}
          <View style={styles.telepon}>
            <View style={styles.boxNomorHape}>
              <TouchableOpacity onPress={() => copyToClipboard()}>
                <Text style={{fontSize: 18}}>{item?.id.substring(0, 10)}</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.caraTelepon}>
              <Image
                source={require('../asset/icon-phone.png')}
                style={styles.iconPhone}
              />
              <Text style={styles.textCaraTelepon}>Telepon</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.caraSMS}>
              <Image
                source={require('../asset/icon-sms.png')}
                style={styles.iconSMS}
              />
              <Text style={styles.textCaraTelepon}>SMS</Text>
            </TouchableOpacity>
          </View>
          {/* Barisan nomor hape selesai di sini */}
          <Separator />
          {/* Barisan Medsos dimulai dari sini*/}
          <View style={{margin: 20}}>
            <Text style={{alignSelf: 'center', fontSize: 12}}>
              Hubungi kawanku via:
            </Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity>
                <Image
                  source={require('../asset/icon-WA.png')}
                  style={styles.iconMedsos}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../asset/icon-IG.png')}
                  style={styles.iconMedsos}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../asset/icon-linkedn.png')}
                  style={styles.iconMedsos}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../asset/icon-tambah-medsos.png')}
                  style={styles.iconMedsos}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../asset/icon-tambah-medsos.png')}
                  style={styles.iconMedsos}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {/* Barisan Medsos selesai di sini*/}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#cecece',
    marginVertical: 5,
  },
  iconBack: {
    height: 30,
    width: 30,
    margin: 20,
  },
  fakeIcon: {
    height: 30,
    width: 30,
    margin: 20,
    tintColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: '#ebdbeb',
  },
  containerColumn: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textNamaLengkap: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  textUmurDanKota: {
    fontSize: 20,
    marginBottom: 5,
  },
  editAndDelete: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 5,
  },
  textDelete: {
    color: '#C70039',
  },
  textDeskripsi: {
    width: 280,
    justifyContent: 'center',
    textAlign: 'center',
    marginTop: 10,
  },
  textBold: {
    fontWeight: 'bold',
  },
  header: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    margin: 10,
  },
  scrollView: {
    padding: 15,
    width: '100%',
    alignSelf: 'center',
  },
  body: {
    backgroundColor: 'white',
    width: '100%',
    marginVertical: 10,
    borderRadius: 20,
    paddingVertical: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 3,
      height: 5,
    },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    shadowRadius: 20,
    elevation: 15,
  },
  pp: {
    height: 90,
    width: 130,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  containerPP: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 40,
  },
  char: {
    height: 90,
    width: 90,
    borderRadius: 50,
  },
  profileUtama: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginVertical: 20,
    marginHorizontal: 20,
    justifyContent: 'center',
  },
  iconPhone: {
    width: 20,
    height: 20,
    tintColor: 'white',
  },
  iconSMS: {
    width: 20,
    height: 20,
    tintColor: 'white',
  },
  telepon: {
    justifyContent: 'space-between',
    marginHorizontal: 20,
    marginVertical: 10,
    flexDirection: 'row',
  },
  boxNomorHape: {
    borderWidth: 1,
    borderColor: '#cecece',
    padding: 20,
    borderRadius: 20,
    marginRight: 2,
  },
  caraTelepon: {
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    marginHorizontal: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textCaraTelepon: {
    fontSize: 10,
    color: 'white',
    marginTop: 5,
  },
  caraSMS: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    marginLeft: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  iconMedsos: {
    width: 50,
    height: 50,
    marginVertical: 10,
    marginRight: 5,
  },
});

export default Profile;
