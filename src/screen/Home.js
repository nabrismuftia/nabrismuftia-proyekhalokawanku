import React, {useEffect, useState} from 'react';
import HomeComponent from '../component/section/HomeComponent';
import Axios from 'axios';
import {useIsFocused} from '@react-navigation/native';
import {BASE_URL, TOKEN} from '../utils/service/Url';

const Home = ({navigation}) => {
  const [contact, setContact] = useState([]);
  const isFocused = useIsFocused();
  useEffect(() => {
    getContact();
  }, [isFocused]);

  const getContact = async () => {
    Axios.get(`${BASE_URL}contact`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('Data Kontak', response);
        setContact(response.data);
      })
      .catch(error => {
        console.log('Error Mengambil data Kontak', error);
      });
  };

  return (
    <HomeComponent
      navigation={navigation}
      contact={contact}
      isFocused={isFocused}
    />
  );
};

export default Home;
