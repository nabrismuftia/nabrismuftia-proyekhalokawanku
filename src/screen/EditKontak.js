import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import PilihAvatar from '../component/section/PilihAvatar';
import Axios from 'axios';
import {BASE_URL, TOKEN} from '../utils/service/Url';

const EditKontak = ({navigation, route}) => {
  const item = route.params.route.params.item;
  console.log('isi item di EditKontak.js', item);

  const [usia, setUsia] = useState('');
  const [imageURLProduk, setImageURLProduk] = useState('');
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [profesi, setProfesi] = useState('');
  const [tentangKawanku, setTentangKawanku] = useState('');
  const [nomorHape, setNomorHape] = useState('');
  const [nomorWA, setNomorWA] = useState('');
  const [akunInstagram, setAkunInstagram] = useState('');
  const [akunLinkedn, setAkunLinkedn] = useState('');
  const [contact, setContact] = useState(null);

  const Separator = () => <View style={styles.separator} />;

  const editContact = async () => {
    const body = [
      {
        id: item.id,
        age: item.age,
        firstName: item.firstName,
        lastName: item.lastName,
        photo: item.photo,
      },
    ];

    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };
    Axios.put(`${BASE_URL}contact/${item.id}`, body, options)
      .then(response => {
        console.log('response edit success', response);
        if (response.status === 200 || response.status === 201) {
          alert('Berhasil mengedit kontak kawan baru');
          navigation.navigate('Home');
          setUsia();
          setNamaDepan();
          setNamaBelakang();
          setImageURLProduk();
          ToastAndroid.showWithGravity(
            'Sebuah kontak kawan baru berhasil diedit',
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
          );
          navigation.goBack();
        }
      })
      .catch(error => {
        console.log('Error mengedit kontak kawan baru', error);
        ToastAndroid.showWithGravity(
          'Error mengedit kontak',
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
        navigation.goBack();
      });
  };

  useEffect(() => {
    if (route.params) {
      const data = route.params.route.params.item;
      console.log('isi data di EditKontak.js', data);
      setUsia(`${data.age}`);
      setNamaDepan(data.firstName);
      setNamaBelakang(data.lastName);
      setImageURLProduk(data.photo);
      console.log('isi age di EditKontak.js', data.age);
    }
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      {/* Header dimulai dari sini */}
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            source={require('../asset/icon-back.png')}
            style={styles.iconBack}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>Kawan Baruku</Text>
        <Image
          source={require('../asset/icon-back.png')}
          style={styles.fakeIcon}
        />
      </View>
      {/* Header Selesai di sini  */}
      <PilihAvatar />
      <ScrollView style={styles.scrollView}>
        <View style={styles.containerAva}>
          <View>
            <Text style={styles.textPilihAvatar}>
              Pilihkan Avatar untuk Kawanmu
            </Text>
            <Separator />
            <View style={styles.inputGambarRow}>
              <View style={styles.containerIconTambahGambar}>
                <Image
                  style={styles.iconTambahGambar}
                  source={require('../asset/icon-tambah-gambar.png')}
                />
              </View>
              <TextInput
                value={imageURLProduk}
                onChangeText={text => setImageURLProduk(text)}
                placeholder="Masukkan Gambar via URL *"
                style={styles.masukkanGambarViaURL}
                keyboardType="default"
              />
            </View>
            <TextInput
              value={namaDepan}
              onChangeText={text => setNamaDepan(text)}
              placeholder="  Nama Depan *"
              style={styles.txtInput}
            />
          </View>
          <View>
            <TextInput
              value={namaBelakang}
              onChangeText={text => setNamaBelakang(text)}
              placeholder="  Nama Belakang *"
              style={styles.txtInput}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '48%'}}>
              <TextInput
                value={usia}
                onChangeText={text => setUsia(text)}
                placeholder="  Usia *"
                keyboardType="numeric"
                style={styles.txtInputMini}
              />
            </View>
            <View style={{width: '48%'}}>
              <TextInput
                value={profesi}
                onChangeText={text => setProfesi(text)}
                placeholder="  Profesi"
                style={styles.txtInputMini}
              />
            </View>
          </View>
          <Text style={styles.textBorang}>
            {'*)'} Borang dengan tanda bintang wajib diisi
          </Text>
          <Separator />
          <View>
            <TextInput
              value={tentangKawanku}
              onChangeText={text => setTentangKawanku(text)}
              multiline
              placeholder="  Tentang Kawanku"
              style={styles.tentangKawanku}
              keyboardType="default"
            />
          </View>
          <Separator />
          <TextInput
            value={nomorHape}
            onChangeText={text => setNomorHape(text)}
            placeholder="  +62 Nomor Ponsel"
            style={styles.txtInput}
          />
          <TextInput
            value={nomorWA}
            onChangeText={text => setNomorWA(text)}
            placeholder="  +62 Nomor WA"
            style={styles.txtInput}
          />
          <TextInput
            value={akunInstagram}
            onChangeText={text => setAkunInstagram(text)}
            placeholder="  instagram.com/"
            style={styles.txtInput}
          />
          <TextInput
            value={akunLinkedn}
            onChangeText={text => setAkunLinkedn(text)}
            placeholder="  linkedn.com/"
            style={styles.txtInput}
          />
          <TouchableOpacity>
            <Image
              source={require('../asset/icon-tambah-medsos.png')}
              style={styles.iconMedsos}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <View style={styles.tombolTambahKawan}>
        <TouchableOpacity style={styles.btnAdd} onPress={editContact}>
          <Text style={styles.textTambahKawan}>Edit Kontak</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#cecece',
    marginVertical: 20,
  },
  iconBack: {
    height: 30,
    width: 30,
    margin: 20,
  },
  fakeIcon: {
    height: 30,
    width: 30,
    margin: 20,
    tintColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: '#ebdbeb',
  },
  judulIsian: {
    fontSize: 16,
    color: '#000',
    fontWeight: '600',
    marginLeft: 10,
  },
  header: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    margin: 10,
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: -350,
  },
  containerAva: {
    width: '100%',
    padding: 15,
  },
  textPilihAvatar: {
    margin: 5,
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '300',
  },
  inputGambarRow: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  containerIconTambahGambar: {
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#dedede',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  iconTambahGambar: {
    tintColor: 'grey',
    width: 30,
    height: 30,
    marginLeft: 5,
  },
  masukkanGambarViaURL: {
    width: '85%',
    height: 50,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#dedede',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  btnAdd: {
    marginVertical: 10,
    width: '90%',
    height: 50,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
    borderRadius: 6,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  btnDlt: {
    marginTop: 20,
    marginLeft: 15,
    width: '15%',
    height: 50,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'maroon',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  txtInput: {
    width: '100%',
    marginTop: 10,
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  txtInputMini: {
    width: '100%',
    marginTop: 10,
    borderRadius: 6,
    borderColor: '#cecece',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  textBorang: {
    color: 'green',
    fontSize: 12,
    marginTop: 20,
    marginLeft: 10,
  },
  tentangKawanku: {
    marginTop: 10,
    width: '100%',
    height: 100,
    borderRadius: 6,
    textAlignVertical: 'top',
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  iconMedsos: {
    width: 50,
    height: 50,
    marginVertical: 10,
    marginRight: 5,
    alignSelf: 'center',
  },
  tombolTambahKawan: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  textTambahKawan: {
    color: '#fff',
    fontWeight: '600',
  },
});

export default EditKontak;
