import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

const Login = ({navigation}) => {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <Image
        style={styles.imgSplash}
        source={require('../asset/img-splash.png')}
      />
      <View style={styles.container2}>
        <View style={styles.headerText}>
          <Text style={styles.textLogin}>Login ke {'\n'}Halo Kawanku</Text>
          <Text style={styles.textNikmati}>
            Nikmati login trial gratis tanpa daftar, tekan 'Masuk'.
          </Text>
          <TextInput
            value={userName}
            onChangeText={text => setUserName(text)}
            placeholder="  Username"
            style={styles.txtInput}
          />
          <TextInput
            value={password}
            onChangeText={text => setPassword(text)}
            placeholder="  Password"
            keyboardType="default"
            secureTextEntry={true}
            style={styles.txtInput}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('ModalLoading2')}
            style={styles.tombolLogin}>
            <Text style={styles.textTombolLogin}>Masuk</Text>
          </TouchableOpacity>
          <Text style={styles.textAtau}>Atau</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('ModalLoading2')}
            style={styles.tombolLoginViaGoogle}>
            <Image
              source={require('../asset/icon-google.png')}
              style={styles.iconLoginViaGoogle}
            />
            <Text>Masuk dengan Google</Text>
          </TouchableOpacity>
          <View style={styles.textTombolLogin2}>
            <Text>Lupa Password? Klik </Text>
            <TouchableOpacity>
              <Text style={{fontWeight: 'bold'}}>di sini</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgSplash: {
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  container2: {
    marginTop: '-135%',
    flexDirection: 'column',
  },
  headerText: {
    backgroundColor: 'white',
    width: 300,
    padding: 20,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  iconLoginViaGoogle: {
    height: 20,
    width: 20,
    margin: 12,
  },
  textNikmati: {
    marginLeft: 5,
    fontWeight: '300',
  },
  textLogin: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  txtInput: {
    width: '100%',
    marginTop: 10,
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  textAtau: {
    alignSelf: 'center',
    marginTop: 15,
    marginBottom: 5,
  },
  tombolLogin: {
    borderWidth: 1,
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  tombolLoginViaGoogle: {
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  textTombolLogin2: {
    flexDirection: 'row',
    marginTop: 20,
    alignSelf: 'center',
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
});
export default Login;
