import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import PilihAvatar from '../component/section/PilihAvatar';

const About = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.imgSplash}
        source={require('../asset/img-splash.png')}
      />
      <View style={styles.container2}>
        <View style={styles.headerText}>
          <Text style={styles.textLogin}>Tentang {'\n'}Halo Kawanku</Text>
          <ScrollView style={styles.scrollView}>
            <PilihAvatar />
            <Text>
              Halo Kawanku merupakan perangkat yang membantu menghubungkan kita
              dengan kawan lama. {'\n'}
              {'\n'}Aplikasi ini dilengkapi dengan fitur pencarian berdasarkan
              profesi dan tautan penghubung untuk mengontak kawan berdasarkan
              kepentingan skill yang dibutuhkan. {'\n'}
              {'\n'}Nikmati desain avatar yang apik dan kawai bikinan poptomatik
              studio. Studio dari Kartasura tersebut didirikan oleh Nabris Mufti
              A. a.k.a nobi, seorang botanikus sekaligus ilustrator yang rajin
              bernarasi. {'\n'}
              {'\n'}Mari kenal lebih lanjut dengan nobi di akun instagramnya,
              @poplilak & @poptomatik Cherio!
            </Text>
          </ScrollView>
          <TouchableOpacity
            onPress={() => navigation.navigate('Home')}
            style={styles.tombolLogin}>
            <Text style={styles.textTombolLogin}>Kembali</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  container2: {
    marginTop: '-135%',
    flexDirection: 'column',
  },
  headerText: {
    backgroundColor: 'white',
    width: 300,
    padding: 20,
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  imgSplash: {
    width: '100%',
    height: '100%',
  },
  scrollView: {height: 200},
  iconLoginViaGoogle: {
    height: 20,
    width: 20,
    margin: 12,
  },
  textLogin: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    marginVertical: 10,
  },
  txtInput: {
    width: '100%',
    marginTop: 10,
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 1,
    elevation: 5,
  },
  tombolLogin: {
    borderWidth: 1,
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  tombolLoginViaGoogle: {
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  textTombolLogin2: {
    flexDirection: 'row',
    marginTop: 20,
    alignSelf: 'center',
  },
  tombolLogin: {
    borderWidth: 1,
    marginTop: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    borderColor: '#cecece',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  textTombolLogin: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
});
export default About;
