import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splashscreen from '../screen/SplashScreen';
import Home from '../screen/Home';
import Profile from '../screen/Profile';
import Login from '../screen/Login';
import KawanBaru from '../screen/KawanBaru';
import About from '../screen/About';
import IzinAksesKontak from '../screen/IzinAksesKontak';
import ModalLoading from '../component/modal/ModalLoading';
import ModalLoading2 from '../component/modal/ModalLoading2';
import EditKontak from '../screen/EditKontak';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splashscreen" component={Splashscreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="ModalLoading2" component={ModalLoading2} />
        <Stack.Screen name="IzinAksesKontak" component={IzinAksesKontak} />
        <Stack.Screen name="ModalLoading" component={ModalLoading} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="KawanBaru" component={KawanBaru} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen name="EditKontak" component={EditKontak} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
