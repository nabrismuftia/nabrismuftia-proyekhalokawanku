import React, {useState} from 'react';
import {
  View,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import Tooltip from 'rn-tooltip';

const ModalFilter = ({show, onClose}) => {
  const chooseFilter = [
    {id: 212, title: 'Profesi'},
    {id: 213, title: 'Lokasi'},
    {id: 214, title: 'A - Z'},
    {id: 215, title: 'Z - A'},
  ];

  return (
    <Modal transparent visible={show} onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.textHeader}>Filter</Text>
          <TouchableOpacity onPress={onClose}>
            <Image
              source={require('../../asset/icon-close.png')}
              style={styles.iconClose}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          {chooseFilter.map(chooseFilter => (
            <TouchableOpacity style={styles.option} key={chooseFilter.id}>
              <Text style={styles.textOption}>{chooseFilter.title}</Text>
              <View style={styles.buletan} />
            </TouchableOpacity>
          ))}
          <Tooltip
            width={120}
            height={80}
            popover={
              <Text style={styles.textToolTip}>
                Filter masih dalam proses pengembangan
              </Text>
            }>
            <View style={styles.tombolCari}>
              <Text style={styles.textTombolCari}>Cari</Text>
            </View>
          </Tooltip>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
    width: '90%',
    paddingHorizontal: 16,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: '#4cb24a',
    borderBottomWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#4cb24a',
  },
  iconClose: {
    height: 20,
    width: 20,
    tintColor: '#4cb24a',
  },
  body: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    paddingHorizontal: 16,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#4cb24a',
    marginHorizontal: 5,
    marginVertical: 10,
  },
  textOption: {
    fontSize: 18,
    color: '#4cb24a',
  },
  selectedOption: {
    backgroundColor: '#E4E7EB',
  },
  tombolCari: {
    margin: 10,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: '#4cb24a',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTombolCari: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  buletan: {
    width: 20,
    height: 20,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#4cb24a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buletanDalem: {
    width: 8,
    height: 8,
    borderRadius: 20,
    backgroundColor: 'blue',
    alignItems: 'center',
  },
  textToolTip: {
    color: 'white',
  },
});

export default ModalFilter;
