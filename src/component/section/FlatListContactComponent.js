import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import Tooltip from 'rn-tooltip';

const FlatListContactComponent = ({navigation, contact, isFocused}) => {
  console.log('isi item di FlatListContactComponent.js', contact);
  const flatListItem = contact.data;
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  return (
    <View>
      {flatListItem ? (
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              isFocused={isFocused}
            />
          }
          data={flatListItem}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate(
                  'Profile',
                  (item = {item}),
                  (isFocused = {isFocused}),
                )
              }
              style={styles.container}>
              <View style={styles.buletanChar}>
                {item?.photo ? (
                  <Image source={{uri: item?.photo}} style={styles.char} />
                ) : (
                  <Image
                    source={{
                      uri: 'https://cdn.vectorstock.com/i/preview-1x/66/14/default-avatar-photo-placeholder-profile-picture-vector-21806614.jpg',
                    }}
                    style={styles.char}
                  />
                )}
                <Image
                  source={require('../../asset/char-2.png')}
                  style={styles.avaMini}
                />
              </View>
              <View style={styles.containerColumn}>
                <Text style={styles.namaPopuler}>
                  {item?.firstName} {item?.lastName}
                </Text>
                <Text>{item?.age} | Programmer</Text>
                <Text style={styles.textNoPonsel}>
                  No. Ponsel:{' '}
                  <Text style={styles.noPonsel}>
                    {item?.id.substring(0, 10)}
                  </Text>
                </Text>
              </View>
              <Tooltip
                width={150}
                height={80}
                popover={
                  <Text style={styles.textToolTip}>
                    Tombil edit dan delete ada di laman "Profile Kawanku"
                  </Text>
                }>
                <Image
                  source={require('../../asset/icon-option.png')}
                  style={styles.iconOption}
                />
              </Tooltip>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={styles.activityIndicator}>
          <ActivityIndicator size={'large'} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '90%',
    marginVertical: 10,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 3,
      height: 5,
    },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    shadowRadius: 20,
    elevation: 15,
  },
  buletanChar: {
    width: 70,
    height: 70,
    backgroundColor: 'white',
    borderRadius: 50,
    marginHorizontal: 10,
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'grey',
  },
  char: {
    height: 60,
    width: 60,
    borderRadius: 50,
  },
  avaMini: {
    width: 50,
    height: 50,
    position: 'absolute',
    bottom: -10,
    right: -20,
  },
  containerColumn: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'flex-start',
    marginLeft: '-13%',
  },
  namaPopuler: {
    fontWeight: 'bold',
    fontSize: 18,
    width: 120,
  },
  textNoPonsel: {
    fontSize: 11,
    marginVertical: 5,
  },
  noPonsel: {
    fontWeight: 'bold',
  },
  textToolTip: {
    color: 'white',
  },
  iconOption: {
    height: 25,
    width: 4,
    margin: 20,
  },
  activityIndicator: {
    marginTop: 200,
    justifyContent: 'center',
  },
});
export default FlatListContactComponent;
