import React from 'react';
import {ScrollView, Image, StyleSheet} from 'react-native';

const PilihAvatar = ({}) => {
  return (
    <ScrollView horizontal={true} style={styles.container}>
      <Image source={require('../../asset/char-1.png')} style={styles.char} />
      <Image source={require('../../asset/char-2.png')} style={styles.char} />
      <Image source={require('../../asset/char-3.png')} style={styles.char} />
      <Image source={require('../../asset/char-4.png')} style={styles.char} />
      <Image source={require('../../asset/char-5.png')} style={styles.char} />
      <Image source={require('../../asset/char-6.png')} style={styles.char} />
      <Image source={require('../../asset/char-7.png')} style={styles.char} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  char: {
    width: 100,
    height: 100,
  },
});
export default PilihAvatar;
