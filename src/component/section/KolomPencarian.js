import React from 'react';
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

const KolomPencarian = ({setModalFilterVisible, onClose}) => {
  return (
    //  Kolom pencarian dimulai dari sini
    <View style={styles.container}>
      <Image
        source={require('../../asset/icon-cari.png')}
        style={styles.iconCari}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Temukan kontak kawanku"
        keyboardType="default"
      />
      <TouchableOpacity
        style={styles.container2}
        onPress={() => setModalFilterVisible(true)}
        onClose={onClose}>
        <Image
          source={require('../../asset/icon-filter.png')}
          style={styles.iconFilter}
        />
      </TouchableOpacity>
    </View>
    /* Kolom pencarian daniconnya selesai di sini */
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    alignSelf: 'center',
    margin: 15,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'grey',
  },
  iconCari: {
    height: 20,
    margin: 15,
    width: 20,
    tintColor: 'grey',
  },
  textInput: {
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: '60%',
  },
  container2: {
    margin: 15,
  },
  iconFilter: {
    height: 20,
    width: 20,
    tintColor: 'grey',
  },
});

export default KolomPencarian;
