import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';

const BottomNavigation = ({navigation}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Image
          source={require('../../asset/icon-home.png')}
          style={styles.iconHome}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('KawanBaru')}>
        <Image
          source={require('../../asset/icon-tambah-kawan.png')}
          style={styles.iconTambahKawan}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('About')}>
        <Image
          source={require('../../asset/icon-about.png')}
          style={styles.iconAbout}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: '#4cb24a',
    position: 'absolute',
    bottom: 0,
  },
  iconHome: {
    height: 40,
    width: 50,
    marginHorizontal: 30,
    marginVertical: 5,
  },
  iconTambahKawan: {
    height: 80,
    width: 80,
    marginHorizontal: 20,
    marginTop: -50,
  },
  iconAbout: {
    height: 50,
    width: 50,
    marginHorizontal: 30,
    marginBottom: 10,
  },
});

export default BottomNavigation;
