import React, {useState} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
import KolomPencarian from '../../component/section/KolomPencarian';
import ModalFilter from '../../component/modal/ModalFilter';
import BottomNavigation from '../../component/section/BottomNavigation';
import FlatListContactComponent from './FlatListContactComponent';

const HomeComponent = ({navigation, contact, isFocused}) => {
  const [modalFilterVisible, setModalFilterVisible] = useState(false);
  console.log('isi item di HomeComponent.js', contact);
  return (
    <View style={styles.container}>
      {/* Header dimulai dari sini */}
      <View style={styles.header}>
        <Text style={styles.headerText}>Menyapa Kawanku </Text>
        <View style={styles.containerKolomPencarian}>
          <KolomPencarian
            modalFilterVisible={modalFilterVisible}
            setModalFilterVisible={setModalFilterVisible}
            onClose={() => setModalFilterVisible(false)}
          />
        </View>
      </View>
      {/* Header Selesai di sini  */}
      <View style={styles.scrollView}>
        <FlatListContactComponent
          navigation={navigation}
          contact={contact}
          isFocused={isFocused}
        />
      </View>
      <BottomNavigation navigation={navigation} />
      <ModalFilter
        show={modalFilterVisible}
        animationType="slide"
        onClose={() => setModalFilterVisible(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ebdbeb',
  },
  header: {
    width: '100%',
    height: 50,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 25,
    color: '#4cb24a',
    fontWeight: 'bold',
    margin: 10,
  },
  containerKolomPencarian: {
    backgroundColor: 'white',
    width: '100%',
  },
  scrollView: {
    marginTop: 80,
    marginVertical: 10,
    paddingBottom: 90,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#ebdbeb',
  },
});

export default HomeComponent;
