/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Home from './src/screen/SplashScreen';
import Routing from './src/navigation/Routing';
import KawanBaru from './src/screen/KawanBaru';
import Profile from './src/screen/Profile';
import Login from './src/screen/Login';
import About from './src/screen/About';
import ModalFilter from './src/component/modal/ModalFilter';
import ModalLoading from './src/component/modal/ModalLoading';
// import LamanCarousel from './src/screen/LamanCarousel';

AppRegistry.registerComponent(appName, () => Routing);
